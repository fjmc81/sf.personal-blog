# sf.personal-blog

This an app built with Symfony 2.3 some years ago. I have been pulling a bit effort to improve and learn about web development and PHP and Symfony, adding a piece of code, as soon as I could to this app.

I could recovered an old package with version 2.8.26. Now it is update and should be works.

## Getting Started

It is pretty easy to have ready this web app in local environment.

If you need to set up any previous data to make it runs in your *localhost*, first at all try to edit the file *[root_folder]*`< /app/config/parameters.yml >` and add/change those paramaters they need to be updated.

Save the file.

### Prerequisites

What things you need to install the software and how to install them

* Composer > 1.0
* PHP >= 7.0

### Installing

Browse to *[root_folder]* by console and run `composer install` command

After a successfull installation, let's set up a minimal and function content to get ready to enjoy this webapp.

From the *[root_folder]* by console, execute the following commands:

`php app/console doctrine:database:create`

We need now to fill up the minimal content to be able to work properly.

`php app/console doctrine:schema:create`

We create our first user to manage the backend of the webapp, as a super-admin user:

`php app/console fos:user:create --super-admin`

Just type the require fields to create your super user, and finally, because in this version, it still has the assectic as front engine bundle, let's runt the command that will create the minimal files to run the current frontend.

To finish this *how to*, type the following command to be sure the URL and run it in your favorite browser.

## Running the tests

Explain how to run the automated tests for this system

## Built With

* [Symfony2](https://symfony.com/doc/2.8/index.html) - The web framework used
* [Bootstrap3](http://getbootstrap.com/docs/3.3/) - The front-end framework used
* [Composer](https://getcomposer.org/doc/) - The front-end framework used

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to this code.

## Versioning

v : 1.2.1

## Author

* **Francisco Carracedo** - *Owner* - [frank-carracedo](https://github.com/frank-carracedo)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgment

This version software has been updated to work properly with PHP 7 versions, and that's what it does.

** NOTE:** PHP >= 7.2 trigge a bug still pending to be solved by bundles' owners involved in this issue.

The workaround I found is just a hotfix, but it makes run the app with no problems.

* Edit `vendor/gregwar/captcha/CaptchaBuilder.php`

* In line 29, replace the value of $textColor *null* by *array()*

* Remember do this after deploy your app, as well as, I only could test it in local environment.
