<?php

namespace Blogger\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
//use Gregwar\CaptchaBundle\Type\CaptchaType;

class SearchType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('search', MultiSearchType::class, array(
					'class' => 'BloggerBlogBundle:Blog', //required
					'search_fields' => array( //optional, if it's empty it will search in the all entity columns
						'title',
						'blog',
						'tags'
					 ), 
					 'comparison_type' => 'wildcard' //optional, what type of comparison to applied ('wildcard','starts_with', 'ends_with', 'equals')
				))
		;
	}
	
	/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blogger\BlogBundle\Entity\Blog'
        ));
    }

    /*public function getName()
    {
        return 'blogger_blogbundle_searchtype';
    }*/
	
}
