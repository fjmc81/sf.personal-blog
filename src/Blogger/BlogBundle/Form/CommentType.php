<?php

namespace Blogger\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class CommentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*$builder
            ->add('user')
            ->add('comment')
            ->add('captcha', CaptchaType::class);
        ;*/
		
		/*
		$builder->add('field', CKEditorType::class, array(
			'config' => array(
				'uiColor' => '#ffffff',
				//...
			),
		));
		*/
		
		$builder->add(
			'user',
			'text',			
			 array(
				'attr' => array(
					'placeholder' => 'Your nickname',
				),
				'label' => false,
				)
			)
			->add(
			'comment',
			'ckeditor',
			array(
				'config_name' => 'front_config',
				'attr' => array(
					'placeholder' => 'Your ideas',
				),
				'label' => false,
				)
			)
			->add('captcha', CaptchaType::class);	
    }

    public function getName()
    {
        return 'blogger_blogbundle_commenttype';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blogger\BlogBundle\Entity\Comment'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'blogger_blogbundle_comment';
    }


}
