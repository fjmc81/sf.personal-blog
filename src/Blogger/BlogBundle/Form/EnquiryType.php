<?php
// src/Blogger/BlogBundle/Form/EnquiryType.php

namespace Blogger\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class EnquiryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
			'name',
			'text', 
			 array(
				'attr' => array(
					'placeholder' => 'Your name',
				),
				'label' => false,
				)
			);
        $builder->add(
			'email',
			'email', 
			 array(
				'attr' => array(
					'placeholder' => 'Your email',
				),
				'label' => false,
				)
			);
        $builder->add(
			'subject',
			'text', 
			 array(
				'attr' => array(
					'placeholder' => 'Subject',
				),
				'label' => false,
				)
			);
		$builder->add('body', 'ckeditor',
			array(
			    'config_name' => 'front_config',
				'attr' => array(
					'placeholder' => 'Idea',
				),
				'label' => false,
				)
		);	
       /* $builder->add(
			'body',
			'textarea', 
			 array(
				'attr' => array(
					'placeholder' => 'Idea',
				),
				'label' => false,
				)
			);*/
        $builder->add('captcha', CaptchaType::class);
    }

    public function getName()
    {
        return 'contact';
    }
}
