<?php
// src/Blogger/BlogBundle/Controller/PageController.php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\Criteria;
use Blogger\BlogBundle\Entity\Enquiry;
use Blogger\BlogBundle\Entity\Blog;
use Blogger\BlogBundle\Entity\Category;
use Blogger\BlogBundle\Entity\Tags;
use Blogger\BlogBundle\Form\EnquiryType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
#use Symfony\Component\Form\Forms;
#use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;

class PageController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()
                   ->getEntityManager();

        $blogs = $em->getRepository('BloggerBlogBundle:Blog')
                    ->getLatestBlogs();

		//$query = $em->createQuery($blogs);
		$paginator  = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$blogs, /* query NOT result */
			$this->get('request')->query->get('page', 1), /*page number*/
			6 /*limit per page*/
		);

        return $this->render('BloggerBlogBundle:Page:index.html.twig', array(
            'blogs' => $pagination
        ));
    }
	public function blogAction()
    {
		$request = $this->getRequest();

        $em = $this->getDoctrine()
                   ->getEntityManager();

        $blogs = $em->getRepository('BloggerBlogBundle:Blog')
                    ->getLatestBlogs();

		//$query = $em->createQuery($blogs);
		$paginator  = $this->get('knp_paginator');
		$pagination = $paginator->paginate(
			$blogs, /* query NOT result */
			$this->get('request')->query->get('page', 1), /*page number*/
			3 /*limit per page*/
		);

        return $this->render('BloggerBlogBundle:Page:blog.html.twig', array(
            'pagination' => $pagination
        ));
    }
	public function categoryAction($categorySlug)
	{
		$em = $this->getDoctrine()->getManager();

		$blogs = $em->getRepository('BloggerBlogBundle:Blog')
			->getBlogsByCategorySlug($categorySlug);

		if (!$blogs) {
			throw $this->createNotFoundException('Unable to find blog posts');
		}

		return $this->render('BloggerBlogBundle:Page:category.html.twig', array(
            'blogs' => $blogs
        ));

		/*return array(
			'blogs'    => $blogs,
		);*/
	}
	public function tagsAction($tagSlug)
	{
		$em = $this->getDoctrine()->getManager();

		$tBlogs = $em->getRepository('BloggerBlogBundle:Blog')
					  ->getBlogsByTagSlug($tagSlug);
					  //->find($tagSlug);

		if (!$tBlogs) {
			throw $this->createNotFoundException('Unable to find blog posts');
		}

		return $this->render('BloggerBlogBundle:Page:tags.html.twig', array(
            'tBlogs' => $tBlogs
        ));
	}
	public function archiveAction()
	{
		$em = $this->getDoctrine()->getManager();

		$aBlogs = $em->getRepository('BloggerBlogBundle:Blog')
						->getLatestBlogs();

		if (!$aBlogs) {
			throw $this->createNotFoundException('Unable to find blog posts');
		}
		
		foreach ($aBlogs as $post) {
            $year = $post->getCreated()->format('Y');
            $month = $post->getCreated()->format('F');
            $archPosts[$year][$month][] = $post;
        }

		return $this->render('BloggerBlogBundle:Page:archive.html.twig', array(
            'archPosts' => $archPosts
        ));
	}
    public function aboutAction()
    {
        return $this->render('BloggerBlogBundle:Page:about.html.twig');
    }
    public function contactAction()
    {
        $enquiry = new Enquiry();
        $form = $this->createForm(new EnquiryType(), $enquiry);

        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact enquiry from francisco-moreno.io')
                    ->setFrom('info@francisco-moreno.io')
                    ->setTo($this->container->getParameter('blogger_blog.emails.contact_email'))
                    ->setBody($this->renderView('BloggerBlogBundle:Page:contactEmail.txt.twig', array('enquiry' => $enquiry)));
                $this->get('mailer')->send($message);

                $this->get('session')->getFlashBag('blogger-notice', 'Your contact enquiry was successfully sent. Thank you!');

                // Redirect - This is important to prevent users re-posting
                // the form if they refresh the page
                return $this->redirect($this->generateUrl('BloggerBlogBundle_contact'));
            }
        }

        return $this->render('BloggerBlogBundle:Page:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }
    public function sidebarAction()
    {
        $em = $this->getDoctrine()
                   ->getEntityManager();

		$feedIo = $this->container->get('feedio');

		//$url = 'http://ep00.epimg.net/rss/tags/ultimas_noticias.xml';
		//$url = 'https://code.tutsplus.com/posts.atom';
		//$feeds = $feedIo->read($url)->getFeed();

        $commentLimit   = $this->container
                           ->getParameter('blogger_blog.comments.latest_comment_limit');
        $latestComments = $em->getRepository('BloggerBlogBundle:Comment')
                           ->getLatestComments($commentLimit);

		$categoryBlogs = $em->getRepository('BloggerBlogBundle:Category')->findAll();

		$tagsBlogs = $em->getRepository('BloggerBlogBundle:Tags')->findAll();
		
		//$archiveBlogs = $em->getRepository('BloggerBlogBundle:Blog')->findBy(array('' =>''));
		
        return $this->render('BloggerBlogBundle:Page:sidebar.html.twig', array(
			'categoryBlogs'		=> $categoryBlogs,
            'latestComments'    => $latestComments,
			//'feeds'				=> $feeds,
            'tagsBlogs'         => $tagsBlogs
        ));

    }
}
